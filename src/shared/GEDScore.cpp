#include "GEDScore.h"
#include "DataHolder.h"
#include "ProteinNode.h"


#define ML 1

// WARNING: The code in this file is a mess.
// Leaving the commented out stuff here because it may give a clue what's going on;
// as the code gets really nasty further down. But speed matters, right?

#ifdef INTERNAL_VERIFICATION
# ifdef _MSC_VER
#  pragma message("INTERNAL_VERIFICATION is on, calculating GED will be VERY slow!")
# else
#  warning "INTERNAL_VERIFICATION is on, calculating GED will be VERY slow!"
# endif
#endif

// for verification only
static void calcPairGED_ultraslow(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged)
{
	const uint n1 = my.nodeId[i];
	const ProteinNode *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const ProteinNode *p2 = ref.node[i];

	switch(!n1 + !n2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			const size_t sz = my.size();
			ASSERT(p1g != p2g);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);

			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] +=ML; 
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] +=ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] +=ML;
			}


			for(size_t j = 0; j < sz; ++j)
			{
				const uint p1_to_my = my.node[j] && p1->hasEdgeTo(my.node[j]);
				const uint my_to_p1 = my.node[j] && my.node[j]->hasEdgeTo(p1);

				const uint p2_to_ref = ref.node[j] && p2->hasEdgeTo(ref.node[j]);
				const uint ref_to_p2 = ref.node[j] && ref.node[j]->hasEdgeTo(p2);

				// Be sure the data in the matrices are really what we expect.
				ASSERT(p1_to_my == col1[my.nodeId[j]]);
				ASSERT(my_to_p1 == row1[my.nodeId[j]]);
				ASSERT(p2_to_ref == col2[ref.nodeId[j]]);
				ASSERT(ref_to_p2 == row2[ref.nodeId[j]]);

				// for debugging
				uint eRemoved = 0, eAdded = 0, eFlipped = 0, eChangedDir = 0, eSubst = 0;

				//                      1               2                4                 8
				const uint mask = (p1_to_my) | (my_to_p1 << 1) | (p2_to_ref << 2) | (ref_to_p2 << 3);

				++ged[mask];

				switch(mask)
				{
					case 0: // no connection at all
						break;

					// directed edges (1) vs nothing -> edge removed
					case 1: // p1->my, nothing on the other side 
					case 2: // my->p1, nothing on the other side
						++eRemoved;
						break;

					// directed edges (2) vs nothing -> edge added
					case 4: // p2->ref, ...
					case 8: // ref->p2, ...
						++eAdded; // one directed edge removed
						break;

					// undirected edge (1) vs nothing -> edge removed
					case 3: // p1<->my, nothing on the other side
						++eRemoved;
						break;

					// undirected edge (2) vs nothing -> edge added
					case 12: // p2<->ref, nothing on the other side
						++eAdded;
						break;

					// same -> substituted
					case 5: // p1->my && p2->ref
					case 10:// my->p1 && ref->p2
					case 15:// my<->p1 && ref<->p2  (all bits set)
						++eSubst;
						break;

					// flipped direction
					case 6: // my->p1, p2->ref
					case 9: // p1->ref, ref->p2
						++eFlipped;
						break;

					// directed -> undirected or vice versa
					case 7: // p1<->my, p2->ref
					case 11:// p1<->my, ref->p2
					case 13:// p1->my, p2<->ref
					case 14:// my->p1, p2<->ref
						++eChangedDir;
						break;

					default:
						ASSERT(false);
						
				}

				bool dummy = false; // DEBUG: breakpoint
			}
		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone // FIXME: not necessarily undirected
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone // FIXME: not necessarily undirected
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

		default: ; // both NULL, nothing to do
	}
}

// slower version but does not require an index
void calcPairGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged)
{
	const uint n1 = my.nodeId[i];
	const ProteinNode *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const ProteinNode *p2 = ref.node[i];

	switch(!n1 + !n2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			ASSERT(p1g != p2g);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);

			// Going for maximum speed here, sanity abandoned.
			// the max. reference node ID is the point where all the NULL entries start in the reference mapping.
			// And we want to run until exactly that point, for now.
			// j will always contain the ID that the corresponding reference node would have:
			// Remember, ref[0] has ID 1, and so on.
			const uint *mptr = &my.nodeId[0]; // Let's hope the compiler makes use of 'register'...
			const uint *end = &my.nodeId[data.GetMaxReferenceNodeID()];

			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] += ML; 
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] += ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] += ML;
			}


			++row2; // Skip first row/column, it's the dummy column
			++col2;
			for( ; mptr != end; ++mptr)
			{
				const uint nodeId = *mptr;
				++ged[GED_MASK(col1[nodeId], row1[nodeId], *col2++, *row2++)];
			}

			end = &my.nodeId[my.size()-1] + 1; // one past the end

			// Keep chugging along...
			// Here, we know we are in the NULL area of ref[].
			// So we only have to look at the variable mapping, because we know it's mapped against NULL.
			for( ; mptr != end; ++mptr)
			{
				const uint nodeId = *mptr;
				++ged[GED_MASK(col1[nodeId], row1[nodeId], 0, 0)];
			}

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

	default: ; // both NULL, nothing to do
	}
}

inline void _fixDoubleCounts_subst(GEDInfo& ged)
{
	ASSERT( (ged[GEDI_SUBST_UNDIRECTED] & 1) == 0 );
	ASSERT( (ged[GEDI_SUBST_DIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_SUBST_DIRECTED_2] & 1) == 0 );
	ASSERT( (ged[GEDI_FLIP_1] & 1) == 0 );
	ASSERT( (ged[GEDI_FLIP_2] & 1) == 0 );
	ASSERT( (ged[GEDI_UNDIRECTED_TO_DIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_UNDIRECTED_TO_DIRECTED_2] & 1) == 0 );
	ASSERT( (ged[GEDI_DIRECTED_TO_UNDIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_DIRECTED_TO_UNDIRECTED_2] & 1) == 0 );

	ged[GEDI_SUBST_UNDIRECTED] >>= 1;
	ged[GEDI_SUBST_DIRECTED_1] >>= 1;
	ged[GEDI_SUBST_DIRECTED_2] >>= 1;
	ged[GEDI_FLIP_1] >>= 1;
	ged[GEDI_FLIP_2] >>= 1;
	ged[GEDI_UNDIRECTED_TO_DIRECTED_1] >>= 1;
	ged[GEDI_UNDIRECTED_TO_DIRECTED_2] >>= 1;
	ged[GEDI_DIRECTED_TO_UNDIRECTED_1] >>= 1;
	ged[GEDI_DIRECTED_TO_UNDIRECTED_2] >>= 1;
}

static void calcPairGEDIndexed_directed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& gedx, const uint *index)
{
	const uint n1 = my.nodeId[i];
	const ProteinNode *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const ProteinNode *p2 = ref.node[i];

	switch(!p1 + !p2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			ASSERT(p1g != p2g);
			ASSERT(p1->group != p2->group);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);

			GEDInfo ged;
			const uint * const refId = &ref.nodeId[0];

			// p1 vs row2i
			// index[nodeId] == position in my[].
			// Then, we know the index at which the node is stored,
			// and can look up the node in ref[], at this index.

			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] += ML; 
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] += ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] += ML;
			}

			if(p1->outgoingv.size())
			{
				const uint *v = &(p1->outgoingv.nodeId[0]);
				const uint *vend = v + p1->outgoingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col1[nodeId] == 1);
					const uint idx = refId[index[nodeId-1]];
					++ged[GED_MASK(1, row1[nodeId], col2[idx], row2[idx])];
				}
			}
			if(p1->incomingv.size())
			{
				const uint *v = &(p1->incomingv.nodeId[0]);
				const uint *vend = v + p1->incomingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row1[nodeId] == 1);
					const uint idx = refId[index[nodeId-1]];
					++ged[GED_MASK(col1[nodeId], 1, col2[idx], row2[idx])];
				}
			}
			if(p1->undirectedv.size())
			{
				const uint *v = &(p1->undirectedv.nodeId[0]);
				const uint *vend = v + p1->undirectedv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col1[nodeId] == 1);
					ASSERT(row1[nodeId] == 1);
					const uint idx = refId[index[nodeId-1]];
					++ged[GED_MASK(1, 1, col2[idx], row2[idx])];
				}
			}

			// p2 vs row1i
			// -- don't need an index vector here.
			// we know that v->nodeId can't be 0, because that would mean
			// a NULL found it's way into the neighbors vector, which is impossible.
			// This figures out the index from the ref node ID (which is always it's ID - 1),
			// and looks at the node on that index in my[].

			const uint * const myId = &my.nodeId[0];

			if(p2->outgoingv.size())
			{
				const uint *v = &(p2->outgoingv.nodeId[0]);
				const uint *vend = v + p2->outgoingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col2[nodeId] == 1);
					const uint idx = myId[nodeId-1];
					++ged[GED_MASK(col1[idx], row1[idx], 1, row2[nodeId])];
				}
			}
			if(p2->incomingv.size())
			{
				const uint *v = &(p2->incomingv.nodeId[0]);
				const uint *vend = v + p2->incomingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row2[nodeId] == 1);
					const uint idx = myId[nodeId-1];
					++ged[GED_MASK(col1[idx], row1[idx], col2[nodeId], 1)];
				}
			}
			if(p2->undirectedv.size())
			{
				const uint *v = &(p2->undirectedv.nodeId[0]);
				const uint *vend = v + p2->undirectedv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col2[nodeId] == 1);
					ASSERT(row2[nodeId] == 1);
					const uint idx = myId[nodeId-1];
					++ged[GED_MASK(col1[idx], row1[idx], 1, 1)];
				}
			}

			// Some edges were seen twice, fix this.
			_fixDoubleCounts_subst(ged);

			// --------

			VERIFY_BLOCK
			{
				GEDInfo ck2, ck3;
				calcPairGED(data, my, ref, i, ck2);
				calcPairGED_ultraslow(data, my, ref, i, ck3);
				ASSERT(ged == ck2);
				ASSERT(ged == ck3);
			}

			// Copy over values that were just calculated.
			// Accumulation needs to be done in an extra array because of
			// _fixDoubleCounts_subst(), see above.
			for(uint i = 0; i < GEDI_MAX; ++i)
				gedx[i] += ged[i];

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(gedx[GEDI_REMOVE_NODE]);
				gedx[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				gedx[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(gedx[GEDI_ADD_NODE]);
				gedx[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				gedx[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

	default: ; // both NULL, nothing to do
	}
}

// slightly faster than function for directed graphs
static void calcPairGEDIndexed_undirected(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged, const uint *index)
{
	const ProteinNode *p1 = my.node[i];
	const ProteinNode *p2 = ref.node[i];

	switch(!p1 + !p2)
	{
		// both pointers valid
		case 0:
		{
			ASSERT(p1->group != p2->group);
			const uchar *row1i = data.GetConnectionsRowInv(p1->group, p1->id); // INVERTED DATA
			//const uchar *row2i = data.GetConnectionsColInv(p2->group, p2->id); // INVERTED DATA
			ASSERT(row1i[0] == 1); // These are dummy entries and known to be 1. INVERTED! 1 == not connected.
			//ASSERT(row2i[0] == 1);
			//const uchar *row1 = data.GetConnectionsRow(p1->group, p1->id);
			const uchar *row2 = data.GetConnectionsCol(p2->group, p2->id);
			//ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);

			// undirected, means: rowX[i] == colX[i]

			// p1 vs row2i
			// index[nodeId] == position in my[].
			// Then, we know the index at which the node is stored,
			// and can look up the node in ref[], at this index.
			//register uint er = 0; // edges removed
			register uint es = 0; // edges substituted
			
			
			
			ged[GEDI_REMOVE_SELF_LOOP]+= ML*(  p1->hasSelfLoop  && (!p2->hasSelfLoop));
			ged[GEDI_ADD_SELF_LOOP]   += ML*(!(p1->hasSelfLoop) &&   p2->hasSelfLoop );
			ged[GEDI_SUBST_SELF_LOOP] += ML*(  p1->hasSelfLoop  &&   p2->hasSelfLoop );

			if(uint p1sz = (uint)p1->allv.size())
			{
				const uint * const refId = &ref.nodeId[0];
				const uint *v = &(p1->allv.nodeId[0]);
				const uint * const vend = v + p1sz;
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row1i[nodeId] == 0); // INVERTED data. Connected means this is 0. And it must be connected.
					//const uint idx = ref[index[v->nodeId-1]].nodeId;
					//er += row2i[idx];
					//ASSERT(row1[v->nodeId] == 1);
					//es += row2[idx];

					//const uint v = row2[idx];
					//es += v;
					//er += (v ^ 1);

					es += row2[refId[index[nodeId-1]]];
				}
				ged[GEDI_REMOVE_UNDIRECTED] += (p1sz - es);
				//ged[GEDI_REMOVE_UNDIRECTED] += er;
				ged[GEDI_SUBST_UNDIRECTED] += es;
			}

			// p2 vs row1i
			// -- don't need an index vector here.
			// we know that v->nodeId can't be 0, because that would mean
			// a NULL found it's way into the neighbors vector, which is impossible.
			// This figures out the index from the ref node ID (which is always it's ID - 1),
			// and looks at the node on that index in my[].
			uint ea = 0; // edges added
			
			if(p2->allv.size())
			{
				const uint * const myId = &my.nodeId[0];
				const uint *v = &(p2->allv.nodeId[0]);
				const uint * const vend = v + p2->allv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					//ASSERT(row2i[v->nodeId] == 0); // INVERTED. 0 == connected, which is known to be so.
					ASSERT(row2[nodeId] == 1);
					ea += row1i[myId[nodeId-1]];
				}
				ged[GEDI_ADD_UNDIRECTED] += ea;
			}

			// --------

			VERIFY_BLOCK
			{
				GEDInfo ck, ck2;
				ck[GEDI_ADD_UNDIRECTED]    = ged[GEDI_ADD_UNDIRECTED];
				ck[GEDI_REMOVE_UNDIRECTED] = ged[GEDI_REMOVE_UNDIRECTED];
				ck[GEDI_SUBST_UNDIRECTED]  = ged[GEDI_SUBST_UNDIRECTED];
				calcPairGEDIndexed_directed(data, my, ref, i, ck2, index); // does more internal verification
				ASSERT(ck == ck2);
			}

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

		default: ; // both NULL, nothing to do
	}
}


void GEDPopulateIndex(const NodeMapping& my, uint *index)
{
	for(size_t i = 0; i < my.size(); ++i)
	{
		const uint id = my.nodeId[i];
		ASSERT(id < my.size()); // This check is very crude and only prevents against values that are really far off.
		if(id)
			index[id-1] = i;
	}
}

void calcWholeGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged)
{
	size_t sz = my.size();
	for(size_t i = 0; i < sz; ++i)
		calcPairGED(data, my, ref, i, ged);
}


// very fast, but needs index
void calcPairGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged, const uint *index)
{
	if(data.HasDirectedEdges())
	{
		calcPairGEDIndexed_directed(data, my, ref, i, ged, index);
	}
	else
	{
		calcPairGEDIndexed_undirected(data, my, ref, i, ged, index);

		VERIFY_BLOCK
		{
			GEDInfo ck;
			calcPairGEDIndexed_directed(data, my, ref, i, ck, index);
			ASSERT(ck == ged);
		}
	}
}

void calcWholeGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged, const uint *index)
{
	size_t sz = my.size();
	if(data.HasDirectedEdges())
	{
		for(size_t i = 0; i < sz; ++i)
			calcPairGEDIndexed_directed(data, my, ref, i, ged, index);
	}
	else
	{
		for(size_t i = 0; i < sz; ++i)
			calcPairGEDIndexed_undirected(data, my, ref, i, ged, index);

		VERIFY_BLOCK
		{
			GEDInfo ck;
			for(size_t i = 0; i < sz; ++i)
				calcPairGEDIndexed_directed(data, my, ref, i, ck, index);
			ASSERT(ck == ged);
		}
	}

	VERIFY_BLOCK
	{
		// (will fail if passed GED isn't clear, but we can safely assume that)
		GEDInfo ck;
		calcWholeGED(data, my, ref, ck);
		ASSERT(ged == ck);
	}
}

#undef ML