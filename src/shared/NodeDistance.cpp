#include "NodeDistance.h"
#include <math.h>
#include <algorithm>

inline score partLogsDistance(score a, score b, uint maxD)
{
	score res = fabs(a - b);
	res /= log(score(maxD + 2));
	ASSERT(res >= 0 && res <= 1);
	return res;
}

inline score partDegreeDistance(uint degA, uint degB)
{
	const uint maxD = std::max(degA, degB);
	return partLogsDistance(log(score(degA + 1)), log(score(degB + 1)), maxD);
}

inline score relativeDegreeDistance(score a, score b)
{
	score res = fabs(a - b);
	res /= score(std::max(a, b));
	ASSERT(res >= 0 && res <= 1);
	return res;
}


score calcNodeDegreeDistance(const ProteinNode *a, const ProteinNode *b)
{
	ASSERT(a && b);
	score res;

	// Half of (#in + #out) ...
	// It's intentional that getDegree*() counts undirected nodes twice.
	//res = partDegreeDistance(a->getDegreeIncoming(), b->getDegreeIncoming());
	//res += partDegreeDistance(a->getDegreeOutgoing(), b->getDegreeOutgoing());

	// this is faster than the above, avoids many log() calls
	
	//printf("abs(%lf -  %lf)/ log(max(%d,   %d))\n", 
	//	a->proto->degreeLogIn, b->proto->degreeLogIn,
	//	a->getDegreeIncoming(), b->getDegreeIncoming());
	//_________________________________________________________
	res  = partLogsDistance(a->proto->degreeLogIn, b->proto->degreeLogIn,
		std::max(a->getDegreeIncoming(), b->getDegreeIncoming()));

	res += partLogsDistance(a->proto->degreeLogOut, b->proto->degreeLogOut,
		std::max(a->getDegreeOutgoing(), b->getDegreeOutgoing()));
	//_________________________________________________________



	//_________________________________________________________
	//res  = relativeDegreeDistance(a->getDegreeIncoming(), b->getDegreeIncoming());

	//res += relativeDegreeDistance(a->getDegreeOutgoing(), b->getDegreeOutgoing());
	//_________________________________________________________


	res /= 2;
	ASSERT(res >= 0 && res <= 1);
	return res;
}

