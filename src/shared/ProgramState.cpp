#include "ProgramState.h"
#include <sstream>
#include <fstream>
#include <signal.h>
#include <algorithm>
#include "tools.h"
#include "ResultPrinter.h"
#include "Threading.h"

// MSVC silliness
#pragma warning(disable: 4355)

ProgramState *ProgramState::_instance = NULL;
bool ProgramState::_mustQuit = false;
bool ProgramState::_isSaving = false;

struct InternalCmd
{
	const char *cmd;
	bool (*func)(const std::string&);
};

// Internal commands, saved in _loadCmds list.
// The rest goes to _extraCmds, which is cleared after serialzation.
static InternalCmd internalVars[] =
{
	{ "GRPN",          /* [groupname0] [groupname1] */  &ProgramState::_Cmd_GRPN               },

	{ "NTW",           /* [filename]                */  &ProgramState::_Cmd_NTW                },
	{ "NTWSIF",        /* [group#] [filename]       */  &ProgramState::_Cmd_NTWSIF             },
	{ "NTWEDGELIST",   /* [group#] [filename]       */  &ProgramState::_Cmd_NTWEDGELIST        },
	{ "NTWEDGELISTH",  /* [group#] [filename]       */  &ProgramState::_Cmd_NTWEDGELISTHEADER  },

	{ "BLASTMTX",      /* [filename]                */  &ProgramState::_Cmd_BLASTMTX     },
	{ "BLASTPAIRLIST", /* [filename]                */  &ProgramState::_Cmd_BLASTPAIRLIST},
	{ "SEQSIM",        /* [group#] [filename]       */  &ProgramState::_Cmd_SEQSIM       },

	{ "GRSIGS",        /* [group#] [filename]       */  &ProgramState::_Cmd_GRSIGS       },

	{ "PREPARE",       /*                           */  &ProgramState::_Cmd_PREPARE      },
	{ "V",             /* [varname] [value]         */  &ProgramState::_Cmd_V            },
	{ "ITERATIONS",    /* [value]                   */  &ProgramState::_Cmd_ITERATIONS   },
	{ "RUNTIMESECS",   /* [value]                   */  &ProgramState::_Cmd_RUNTIMESECS  },

	{ NULL, NULL }
};

static bool isInternalCmd(const std::string& cmd)
{
	for(InternalCmd *v = &internalVars[0]; v->cmd; ++v)
		if(cmd == v->cmd)
			return true;
	return false;
}

static bool execInternalCmd(const std::string& cmd, const std::string& params)
{
	for(InternalCmd *v = &internalVars[0]; v->cmd; ++v)
		if(cmd == v->cmd)
			return v->func(params);
	return false;
}

ProgramState::ProgramState(ThreadPool *thpool)
 : workObject(NULL)
 , dh(settings)
 , ph(settings)
 , thpool(thpool)
 , _logger(*this)
 , _iterationCount(0)
 , _startTime(0)
 , _iterationsWithoutScoreChange(0)
 , _lastScore(9999999)
 , _startIterationCount(0)
 , _totalRunningTime(0)
 , _lastSaveTime(0)
 , _seriesCounter(0)
{
	ASSERT(!_instance);
	_instance = this;
	dh.SetThreadPool(thpool);
}

ProgramState::~ProgramState()
{
	_instance = NULL;
}

ProgramState& ProgramState::GetInstance()
{
	ASSERT(_instance);
	return *_instance;
}

void ProgramState::_OnSignal(int sig)
{
	switch(sig)
	{
		case SIGINT:
			if(_mustQuit && !_isSaving) // no abort while saving, to prevent data loss
			{
				printf("Exit forced, state not saved, bye!\n");
				raise(SIGTERM);
				exit(2);
			}
			_mustQuit = true;
			printf("Caught exit signal... going to save state.\n");
	}
}

void ProgramState::Shutdown()
{
	printf("ProgramState: Shutdown...\n");
	_logger.close();
	SaveData();
	printf("ProgramState: Shutdown complete.\n");
}

void ProgramState::SaveData()
{
	std::string fn;
	if(settings.saveStateName.empty())
		fn = generateTimestampStr(&_startTime);
	else
		fn = settings.saveStateName;

	if(settings.outPrefix.size())
		addToFileName(fn, settings.outPrefix);

	if(settings.saveSeries)
	{
		char buf[32];
		sprintf(buf, "_%05u", _seriesCounter);
		++_seriesCounter;
		addToFileName(fn, buf);
	}

	std::string outfn = fn + ".matching";
	std::ofstream outf(outfn.c_str()); 
	if(outf)
	{
		printf("Writing results to '%s' ...\n", outfn.c_str());
		ResultPrinter::AsList(*this, outf);
		outf.close();
	}
	else
	{
		printf("Failed to open '%s' for writing!\n", outfn.c_str());
	}

	SaveState(fn);
}

bool ProgramState::Init(IProblemSolver& wobj)
{
	workObject = &wobj;
	HookSignals();

	if(thpool)
		thpool->start(settings.numThreads);

	bool good = false;
	if(settings.loadStateName.length())
	{
		if(Load(settings.loadStateName))
		{
			if(!workObject->IsEmpty() || settings.acceptEmptyState)
			{
				printf("=== Loaded state ===\n");
				good = true;
			}
			else
			{
				printf("Error: Empty state (use --accept-empty to load it anyway if this is intended)\n");
			}
		}
		else if(settings.createStateIfNotExist)
		{
			printf("=== Creating new state ===\n");
			good = true;
		}
		else
		{
			printf("Error: --resume specified, but failed to load state (use --create to override)\n");
		}

		if(!good)
			return false;
	}

	if(!dh.IsInitialized()) // was Prepare() called? (is a command in the state file also)
	{
		if(Prepare())
		{
			printf("State prepared and ready to go!\n");
		}
		else
		{
			printf("ERROR: ProgramState::Prepare() failed!\n");
			return false;
		}
	}

	if(settings.logger_iterations && settings.logger_file.length())
	{
		if(_logger.open(settings.logger_file.c_str()))
			printf("Opened log file '%s', logging every %u iterations.\n",
				settings.logger_file.c_str(), settings.logger_iterations);
		else
			printf("WARNING: Failed to open log file '%s'\n", settings.logger_file.c_str());
	}
	else
	{
		printf("Logging disabled.\n");
	}

	_startTime = time(NULL); // FIXME: use something better
	_startIterationCount = _iterationCount;

	workObject->Init();
	return true;
}

void ProgramState::NextIteration()
{
	_logger.log();

	++_iterationCount;

	const score sc = workObject->getBestAgent()->getScore();
	if(sc < _lastScore)
	{
		_iterationsWithoutScoreChange = 0;
		_lastScore = sc;
	}
	else
		++_iterationsWithoutScoreChange;

	if(settings.autosaveSecs)
	{
		const uint runtime = GetRunningTime();
		const uint runtimeWithoutSave = runtime - _lastSaveTime;
		if(runtimeWithoutSave >= settings.autosaveSecs)
		{
			SaveData();
			_lastSaveTime = runtime;
		}
	}
}

uint ProgramState::GetRunningTime() const
{
	return uint(time(NULL) - _startTime);
}

bool ProgramState::IsDone()
{
	if(settings.abort_seconds)
	{
		if(GetRunningTime() > settings.abort_seconds)
			return true;
	}

	if(settings.abort_iterations && ((_iterationCount - _startIterationCount) >= settings.abort_iterations))
		return true;

	if(settings.abort_maxIterationsWithoutScoreChange && workObject)
	{
		if(_iterationsWithoutScoreChange >= settings.abort_maxIterationsWithoutScoreChange)
			return true;
	}

	return false;
}



void ProgramState::AddCmd(const std::string& cmd, const std::string& params, bool early /* = false */)
{
	(early ? _earlySaveList : _saveList).push_back(cmd + ' ' + params);
}

bool ProgramState::Load(const std::string& fn)
{
	if(fn.empty())
	{
		printf("ProgrmState: No state file specified, starting fresh.\n");
		return false;
	}

	std::ifstream in(fn.c_str());
	if(!in)
	{
		printf("ProgramState: State file not found: %s\n", fn.c_str());
		return false;
	}

	printf("ProgramState: Loading state '%s'\n", fn.c_str());

	std::string line;
	uint c = 0;
	while(std::getline(in, line))
	{
		chompNewline(line);
		++c;
		if(!_ExecOneLine(line))
		{
			printf("ProgramState: Failed to exec \"%s\" (line %u)\n", line.c_str(), c);
		}
	}

	return true;
}

bool ProgramState::SaveState(const std::string& fn)
{
	if(settings.noSave)
		return false;

	if(fn.empty())
	{
		printf("ProgramState::Save() ERROR: No file name set!\n");
		return false;
	}
	_isSaving = true;

	printf("ProgramState: Saving state to '%s' ...\n", fn.c_str());

	_earlySaveList.clear();
	_saveList.clear();

	// -- Start data serialization --

	settings.serialize(*this);

	if(workObject)
		workObject->serialize(*this);

	std::ofstream out(fn.c_str());
	if(!out)
	{
		printf("ProgramState: FAILED to open file for writing: %s\n", fn.c_str());
		return false;
	}

	out << "ITERATIONS " << _iterationCount << std::endl;
	out << "RUNTIMESECS " << (_totalRunningTime + GetRunningTime()) << std::endl;

	// Save config and variables
	for(std::list<std::string>::iterator it = _earlySaveList.begin(); it != _earlySaveList.end(); ++it)
		out << *it << std::endl;

	// Save commands generated by calling ProgramState member methods
	for(std::list<std::string>::iterator it = _manualList.begin(); it != _manualList.end(); ++it)
		out << *it << std::endl;

	// Save everything else that was serialized on the way
	for(std::list<std::string>::iterator it = _saveList.begin(); it != _saveList.end(); ++it)
		out << *it << std::endl;

	out.close();
	_isSaving = false;

	printf("State saved.\n");
	return true;
}

void ProgramState::HookSignals()
{
	signal(SIGINT, _OnSignal);
}

bool ProgramState::Prepare()
{
	// apply config variables from override list
	for(std::list<std::string>::iterator it = _overrideList.begin(); it != _overrideList.end(); ++it)
	{
		if(!this->_Cmd_V(*it)) // hackish
		{
			printf("ProgramState: Unknown command-line setting: \"%s\"\n", it->c_str());
			return false;
		}
	}

	if(!settings.CheckAndApply())
	{
		printf("!! Settings check failed, settings are not sane and will cause problems.\n");
		return false;
	}

	// Crosslink internal structures
	dh.SetProteinHolder(&ph);

	// Set everything up, precalc data, etc.
	if(!ph.Prepare())
		return false;

	if(!dh.Precalc())
		return false;

	_AddToManualList("PREPARE");

	return true;
}

bool ProgramState::HandleCommandlineCmd(const char *str)
{
	if(!str || !*str)
		return false;

	const std::string line = str;
	size_t eq = line.find('=');
	if(eq == std::string::npos)
		return false;

	// do not allow spaces here
	size_t sp = line.find(' ');
	if(sp != std::string::npos)
		return false;

	std::string cmd = line.substr(0, eq);
	std::string param = line.substr(eq + 1);

	_overrideList.push_back(cmd + ' ' + param);
	return true;
}

bool ProgramState::_ExecOneLine(const std::string& line)
{
	// comment/empty?
	if(line.empty() || line[0] == '#')
		return true; // nothing bad

	size_t pos = line.find(' ');
	if(pos == std::string::npos) // no params?
	{
		return _ExecCommand(line, "");
	}

	std::string cmd = line.substr(0, pos);
	std::string params = line.substr(pos + 1);

	return _ExecCommand(cmd, params);
}

bool ProgramState::_ExecCommand(const std::string& cmd, const std::string& params)
{
	if(isInternalCmd(cmd))
	{
		return execInternalCmd(cmd, params);
	}

	if(workObject)
		return workObject->unserialize(cmd, params);

	return false;
}

void ProgramState::_AddToManualList(const std::string& s)
{
	std::list<std::string>::iterator it = std::find(_manualList.begin(), _manualList.end(), s);
	if(it == _manualList.end())
		_manualList.push_back(s);
}

bool ProgramState::ReadBLASTMatrix(const std::string& fn)
{
	settings.blastScoreFile = fn;
	if(ph.ReadBLASTMatrix(fn))
	{
		_AddToManualList("BLASTMTX " + fn);
		return true;
	}
	return false;
}

bool ProgramState::ReadBLASTPairList(const std::string& fn, int left_group)
{
	settings.blastScoreFile = fn;
	if(ph.ReadBLASTPairList(fn, left_group))
	{
		_AddToManualList("BLASTPAIRLIST " + fn);
		return true;
	}
	return false;
}


bool ProgramState::_Cmd_BLASTMTX(const std::string& params)
{
	return GetInstance().ReadBLASTMatrix(params);
}

bool ProgramState::_Cmd_BLASTPAIRLIST(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());

	return GetInstance().ReadBLASTPairList(params, group);
}


void ProgramState::SetGroupNames(const std::string& a, const std::string& b)
{
	ph.SetGroupNames(a, b);
	_AddToManualList("GRPN " + a + ' ' + b);
}
bool ProgramState::_Cmd_GRPN(const std::string& params)
{
	size_t pos = params.find(' ');
	if(pos == std::string::npos)
		return false;

	std::string a = params.substr(0, pos);
	std::string b = params.substr(pos + 1);

	GetInstance().SetGroupNames(a, b);
	return true;
}


bool ProgramState::ReadGraphletSigs(const std::string& fn, uint group)
{
	if(ph.ReadGraphletSigs(fn, group))
	{
		std::ostringstream strm;
		strm << "GRSIGS " << group << ' ' << fn;
		_AddToManualList(strm.str());
		return true;
	}
	return false;
}
bool ProgramState::_Cmd_GRSIGS(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());
	return GetInstance().ReadGraphletSigs(fn, group);
}


bool ProgramState::ReadNetwork(const std::string& fn)
{
	if(ph.ReadNetwork(fn))
	{
		_AddToManualList("NTW " + fn);
		return true;
	}
	return false;
}
bool ProgramState::_Cmd_NTW(const std::string& params)
{
	return GetInstance().ReadNetwork(params);
}


bool ProgramState::ReadNetworkSIF(const std::string& fn, uint groupIdx)
{
	if(ph.ReadNetworkSIF(fn, groupIdx))
	{
		std::ostringstream strm;
		strm << "NTWSIF " << groupIdx << ' ' << fn;
		_AddToManualList(strm.str());
		return true;
	}
	return false;
}

bool ProgramState::_Cmd_NTWSIF(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());
	return GetInstance().ReadNetworkSIF(params, group);
}

bool ProgramState::ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader)
{
	if(ph.ReadNetworkEDGELIST(fn, groupIdx, hasHeader))
	{
		std::ostringstream strm;
		strm << "NTWEDGELIST" << (hasHeader ? "H " : " ") << groupIdx << ' ' << fn;
		_AddToManualList(strm.str());
		return true;
	}
	return false;
}

bool ProgramState::_Cmd_NTWEDGELIST(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());
	return GetInstance().ReadNetworkEDGELIST(params, group, false);
}

bool ProgramState::_Cmd_NTWEDGELISTHEADER(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());
	return GetInstance().ReadNetworkEDGELIST(params, group, true);
}


bool ProgramState::ReadSeqSimData(const std::string& fn, uint groupIdx)
{
	// FIXME: The actual file is loaded later in DataHolder,
	// so we can't fail here if the file is malformed.
	// This is not so nice...

	settings.seqsimMatrixFile = fn;
	settings.seqsimMatrixLeftgroup = groupIdx;

	std::ostringstream strm;
	strm << "SEQSIM " << groupIdx << ' ' << fn;
	_AddToManualList(strm.str());
	return true;
}
bool ProgramState::_Cmd_SEQSIM(const std::string& params)
{
	std::string grpstr, fn;
	if(!splitString(params, grpstr, fn))
		return false;

	uint group = atoi(grpstr.c_str());
	return GetInstance().ReadSeqSimData(fn, group);
}


bool ProgramState::_Cmd_PREPARE(const std::string& params)
{
	return GetInstance().Prepare();
}

bool ProgramState::_Cmd_V(const std::string& params)
{
	std::string var, val;
	if(!splitString(params, var, val))
		return false;
	return GetInstance().settings.unserialize(var, val);
}

bool ProgramState::_Cmd_ITERATIONS(const std::string& params)
{
	GetInstance()._iterationCount = atoi(params.c_str());
	return true;
}

bool ProgramState::_Cmd_RUNTIMESECS(const std::string& params)
{
	GetInstance()._totalRunningTime = atoi(params.c_str());
	return true;
}

