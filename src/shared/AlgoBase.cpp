#include "AlgoBase.h"
#include "Threading.h"
#include "DataHolder.h"


AlgoBase::AlgoBase(AlgoID algoid, const DataHolder& dh, ThreadPool *thpool)
 : _thpool(thpool)
 , _dataH(dh)
 , _settings(dh.GetUserSettings())
 , _rng(NULL)
 , _mgr(NULL)
 , _algoid(algoid)
{
	if(!thpool)
		_rng = new MTRand(); // Fallback mode: If single-threaded, we need an own RNG
}

AlgoBase::~AlgoBase()
{
	ASSERT(!_mgr);
	if(_rng)
		delete _rng;
}


static void _CalcAgentScore_MT(MTRand& /*unused*/, void *p, void *, void*)
{
	ASSERT(((Agent*)p)->sanityCheck());

	// Make sure the compiler doesn't optimize this out because it's const
	volatile score f = ((Agent*)p)->getScore();

	// For sorting.
	// TODO: Should we calc this everytime?
	// It's quite likely this value isn't used if an Agent
	// is created and dropped again between two duplicate removals.
	// But anyway, the hash function used is *very* fast,
	// and adding an extra threaded function for this
	// would take longer than simply doing it single-threadedly
	// because of mutex locking overhead.
	// So we just do it here while we are in a thread func that needs to be called anyway.
	((Agent*)p)->getMappingNonConst().doHash();
}

// Unified score calculation function. Uses multiple threads to achieve better speed.
void AlgoBase::_CalcScores()
{
	_mgr->AssertCompacted();
	AgentArray& agents = _mgr->agents;

	size_t sz = agents.size();

	if(_thpool)
	{
		// Multi-threaded - defer work to thread pool
		_thpool->assertIdle();
		_thpool->lock();
		for(size_t i = 0; i < sz; ++i)
			if(agents[i] && agents[i]->isDirty())
				_thpool->addJobUnsafe(_CalcAgentScore_MT, agents[i]);
		_thpool->unlock();
		_thpool->waitUntilEmpty();
	}
	else
	{
		// Single-threaded - just calculate here
		for(size_t i = 0; i < sz; ++i)
		{
			Agent *a = agents[i];
			if(a && a->isDirty())
			{
				_CalcAgentScore_MT(*_rng, agents[i], NULL, NULL);
			}
		}
	}
}

void AlgoBase::Link(AgentMgr& mgr)
{
	ASSERT(!_mgr);
	_mgr =  &mgr;
}

void AlgoBase::Unlink()
{
	ASSERT(_mgr);
	_OnUnlink();
	_mgr = NULL;
}

void AlgoBase::serialize(ProgramState& state)
{
	ASSERT(_mgr);

	_mgr->serialize(state);
}

bool AlgoBase::unserialize(const std::string& cmd, const std::string& params)
{
	ASSERT(_mgr);

	return _mgr->unserialize(cmd, params);
}
