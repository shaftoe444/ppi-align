#ifndef PTOOLS_H
#define PTOOLS_H

#include "common.h"

void chompNewline(std::string& s);
void fixProteinName(std::string& s);
uint adler32(uint adler, const char *buf, uint len);
void debugBreak();
bool splitString(const std::string& src, std::string& a, std::string& b);
void strToLower(std::string& s);
void strToUpper(std::string& s);
std::string generateTimestampStr(const time_t *usetime);
void addTimestampToFileName(std::string& fn, const time_t *usetime);
void addToFileName(std::string& fn, const std::string& add);

#endif
