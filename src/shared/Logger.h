#ifndef LOGGER_H
#define LOGGER_H

#include "common.h"
#include <stdio.h>

/*
 * call log in each step
 * appends to file
 * format of logged stuff is:
 * [iteration]	[GED]	[EC]	[PS]	[raw GED]	[partial GED]	[running time]
 *
 */

class ProgramState;

class Logger
{
public:
	Logger(const ProgramState& state);
	~Logger();

	bool open(const char *fn, bool append = true);
	void log();
	void close();

private:

	FILE *_file;
	uint _linesWritten;
	uint _every;
	const ProgramState& state;
};

#endif
