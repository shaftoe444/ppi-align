#ifndef COMMON_H
#define COMMON_H

// Stupid MSVC warnings
#ifndef  _CRT_SECURE_NO_WARNINGS
#  define  _CRT_SECURE_NO_WARNINGS
#endif

#ifndef  _CRT_SECURE_NO_DEPRECATE
#  define  _CRT_SECURE_NO_DEPRECATE
#endif

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include "PlatformSpecific.h"

#if defined(_WIN32) || defined(_MSC_VER)
#  include <malloc.h>
#else
#  include <stdlib.h>
	// for intptr_t
#  include <sys/types.h>
#  include <stdint.h>
#endif

#ifdef NDEBUG
#  define ASSERT(x)
#else
#  define ASSERT(x) { if(!(x)) { fprintf(stderr, "ASSERTION FAILED: \"%s\", File: %s:%u\n", #x, __FILE__, __LINE__); TriggerBreakpoint(); } }
#endif

// Define this for thorough (and extremely slow) verification of internal calculations.
#ifdef INTERNAL_VERIFICATION
#  define VERIFY(x) ASSERT(x)
#  define VERIFY_BLOCK if(1)
#else
#  define VERIFY(x)
#  define VERIFY_BLOCK if(0)
#endif

typedef unsigned int uint;
typedef unsigned char uchar;
typedef double score;


#endif
