#ifndef PROTEIN_HOLDER_H
#define PROTEIN_HOLDER_H

#include "common.h"
#include "Matrix.h"
#include <vector>
#include <map>
#include <set>
#include "ProteinNode.h"

class UserSettings;

class ProteinHolder
{
public:
	ProteinHolder(const UserSettings& settings);
	~ProteinHolder();

	void SetGroupNames(const std::string& a, const std::string& b);

	bool ReadNetwork(const std::string& fn);
	bool ReadNetworkSIF(const std::string& fn, uint groupIdx); // for easy cytoscape import
	bool ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader); // for easy cytoscape import

	bool ReadBLASTMatrix(const std::string& fn, bool useBinary = true);
	int getIndex(uint g, std::string name, uint & good, uint & fails, ProteinNode * p);
	bool ReadBLASTPairList(const std::string& fn, int left_group, bool useBinary = true);

	bool ReadGraphletSigs(const std::string& fn, uint group);

	void Add(ProteinNode *p);
	
	// returns pointer to ProteinNode by its name 
	ProteinNode *Get(uint group, const std::string& name, bool create = false);

	inline uint GetGroupIdx(const std::string& s) const
	{
		if(s == _grpNames[0])
			return 0;
		if(s == _grpNames[1])
			return 1;
		return -1;
	}

	inline const PNodeArray& GetGroup(uint g) const { return _grpV[g]; }
	inline const char *GetGroupName(uint g) const { return _grpNames[g].c_str(); }

	bool Prepare();

	inline score GetBlastScore(const ProteinNode *a, const ProteinNode *b) const
	{
		ASSERT(_wantBLASTMatrix);
		ASSERT(a && b);
		ASSERT(a->group != b->group);

		// TAKE CARE: THE MATRIX IS STORED WITH X AND Y AXES INVERTED!!
		// (not my problem!)

		if(a->group == 0)
			return _blast(b->blastMtxId, a->blastMtxId);
		else
			return _blast(a->blastMtxId, b->blastMtxId);
	}

	inline const Matrix<uchar>& GetConnMatrix(uint group)
	{
		return _conn[group];
	}

	// Drops some internal matrices no longer needed after setting up DataHolder. Called by DataHolder.
	void Cleanup();

	bool UsingBLASTData() const { return _wantBLASTMatrix; }
	bool UsingGraphletData() const { return _wantGraphletSigs; }


protected:

	bool _ReadBLASTMatrixBin(const std::string& fn);
	bool _WriteBLASTMatrixBin(const std::string& fn);
	NodePrototype *_GetProto(const std::string& name, uint group); // creates if not existing
	bool _AddPairByName(uint groupIdx, const std::string& name1, const std::string& name2); // adds one edge name1 -> name2
	void _SetupConnectivityMatrix(uint g);
	bool _ReadConnectivityMatrixBin(uint g);
	bool _WriteConnectivityMatrixBin(uint g);
	void _PrecalcGraphletLogs();
	void _PrecalcDegreeLogs();
	void _SetupNodeIDs();

	Matrix<score> _blast; // blast scores (uses ProteinNode::blastMtxId)
	score initBlastValue;
	score defaultBlastValue;

	Matrix<uchar> _conn[2];    // connectivity (uses ProteinNode::id) - filled in Prepare(). *symmetric*

	ProtoMap _protos[2];    // maps name -> prototype
	PNodeArray _grp[2];  // maps BLAST ID -> node. Temporary.
	PNodeMap _grpN[2];   // maps name -> node
	PNodeArray _grpV[2]; // contains nodes sorted by their ID. Filled in Prepare(), mainly used by DataHolder
	std::string _grpNames[2];

	bool _wantGraphletSigs;
	bool _wantBLASTMatrix;

	const UserSettings& _settings;
};



#endif
