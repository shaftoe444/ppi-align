#ifndef PROTEIN_NODE_H
#define PROTEIN_NODE_H

#include "common.h"

#define GRAPHLET_SIGS 73

#include "Node.h"
#include <vector>
#include <map>

#include "MurmurHash3.h"

struct NodePrototype;
class ProteinNode;

typedef std::map<std::string, NodePrototype*> ProtoMap;
typedef std::vector<ProteinNode*> PNodeArray;
typedef std::map<std::string, ProteinNode*> PNodeMap;


struct NodePrototype
{
	NodePrototype() : hasSigs(false) {}

	std::string name;
	uint graphlets[GRAPHLET_SIGS];
	score graphletLogs[GRAPHLET_SIGS];
	bool hasSigs;

	score degreeLogIn;
	score degreeLogOut;
};

struct NodeMapping
{
	NodeMapping()
	: _hashed(false)
	{
	}

	// always same size
	std::vector<uint> nodeId;
	std::vector<const ProteinNode*> node;

	inline size_t size() const
	{
		ASSERT(node.size() == nodeId.size());
		return nodeId.size();
	}
	inline void swap(size_t a, size_t b)
	{
		std::swap(nodeId[a], nodeId[b]);
		std::swap(node[a], node[b]);
	}
	inline void reserve(size_t sz)
	{
		nodeId.reserve(sz);
		node.reserve(sz);
	}
	inline void clear()
	{
		nodeId.clear();
		node.clear();
	}
	inline void resize(size_t sz)
	{
		nodeId.resize(sz, 0);
		node.resize(sz, NULL);
	}
	inline void push_back(const ProteinNode *p); // defined below

	inline bool operator==(const NodeMapping& o) const
	{
		//return nodeId == o.nodeId; // accurate method

		ASSERT(_hashed);
		ASSERT(o._hashed);
		return !memcmp(&_hash[0], &o._hash[0], sizeof(_hash));
	}

	// for sorting
	inline bool operator<(const NodeMapping& o) const
	{
		//return nodeId < o.nodeId; // accurate method

		ASSERT(_hashed);
		ASSERT(o._hashed);
		return memcmp(&_hash[0], &o._hash[0], sizeof(_hash)) < 0;
	}

	inline void doHash()
	{
		if(!_hashed)
		{
			MurmurHash3_x64_128(&nodeId[0], size() * sizeof(uint), 0, &_hash[0]);
			_hashed = true;
		}
	}

private:
	unsigned char _hash[16]; // 2x uint64
	bool _hashed;


};

class ProteinNode : public Node
{
public:
	ProteinNode() : id(0), group(-1), blastMtxId(-1), prematched(false) {}

	uint id; // internal ID (per group). For any actual node, this is never 0.
	uint group; // internal group ID (0 or 1) - used as array index!

	const NodePrototype *proto;
	uint blastMtxId; // array index in holder's _blast matrix [0...n] -- not used later on

	inline const std::string& name() const { ASSERT(proto); return proto->name; }
	// HMM: add fullname?

	// -- Note: --
	// These vectors differ from the sets in Node.h! Here, outgoing/incoming does NOT contain undirected edges!
	// (sorted by name (same as the originating sets), used for fast GED lookup)
	NodeMapping allv; //  all neighbors 

	NodeMapping outgoingv; // this ---> other, strictly
	NodeMapping incomingv; // this <--- other, strictly

	NodeMapping undirectedv; // this <--> other

	bool prematched; // true if there is a predetermined partner for this node. Used by ProteinHolder only.
};

inline void NodeMapping::push_back(const ProteinNode *p)
{
	nodeId.push_back(p ? p->id : 0);
	node.push_back(p);
}

#endif
