#include "CmdHelp.h"
#include "common.h"

namespace CmdHelp
{


void Print()
{
	puts(
		"--groups <Gr0> <Gr1>     * Set network group names.\n"
		"\n"
		"--ntw <file>             * Read network file, NTW format.\n"
		"--sif <file> <GroupName> * Read network file, SIF format.\n"
		"--edgelist <file> <GroupName>  * Read network file, EDGELIST format.\n"
		"--edgelisth <file> <GroupName> * Read network file, EDGELISTH format.\n"
		"--grsig <file> <GroupName>     * Load a graphlet counts file.\n"
		"                                 The <GroupName> is one of those passed via\n"
		"                                 --groups, and must be specified.\n"
		"\n"
		"--blast <file>           * Read a matrix with BLAST scores.\n"
		"--blastmatrix <file>     * Read a matrix with BLAST scores.\n"
		"--blastpairlist <file>   * Read a BLAST scores matrix as 3-column pairs list.\n"
		"\n"
		"--pop <N>                * Number of new individuals per iteration.\n"
		"                           (default = 400)\n"
		"--threads <N>            * Number of threads to use.\n"
		"                           (default = number of CPU cores)\n"
		"--undirected             * Treat networks as undirected.\n"
		"--maxiter <N>            * Stop after N iterations.\n"
		"--maxsecs <N>            * Stop after N seconds.\n"
		"--maxsame <N>            * Stop after N iterations without\n"
		"                            any score improvement.\n"
		"--config <opt>=<value>   * Set internal config option, e.g.\n"
		"                           \"--config weightGraphlets=0.5\".\n"
		"                           Alternatively, use -c in short.\n"
		"                           The param=value block must not contain spaces.\n"
		"                           See README file for a list of supported options.\n"
		"--no-workfiles           * Do not dump matrices to disk for re-use.\n"
		"--no-prematch            * Do not assume that nodes with the same name\n"
		"                           (case sensitive!) in both networks should be\n"
		"                           always matched.\n"
		"--logfile <file>         * Save statistics to file. (default: no logging)\n"
		"--logiter <N>            * Save one log entry each N iterations.\n"
		"                           Note that the first 200 iterations are always\n"
		"                           logged regardless of this setting. (default = 10)\n"
		"State related:\n"
		"--resume <file>          * Load a state dump and continue working on it.\n"
		"                           Upon exit, this state file will be overwritten,\n"
		"                           unless --no-save is given.\n"
		"--create                 * By default, --resume aborts if the state file\n"
		"                           does not exist. Adding --create will resume a state\n"
		"                           if it exists, and create a new one otherwise.\n"
		"--save <file>            * Specify state file name. If not given, this is\n"
		"                           either the file name passed to --resume,\n"
		"                           or a file name derived from a timestamp.\n"
		"                           The name of the results file is this + \".matching\".\n"
		"--autosave <N>           * Save state + results txt every N seconds.\n"
		"--no-save                * Do not save state before exiting, only results txt.\n"
		"--series                 * If used, autosave will create a new file every save.\n"
		"\n"
		);
}


}; // end namespace CmdHelp

