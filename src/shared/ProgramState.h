#ifndef PROGRAMSTATE_H
#define PROGRAMSTATE_H

#include <list>
#include <ctime>

#include "ProteinHolder.h"
#include "DataHolder.h"
#include "AgentMgr.h"
#include "AlgoBase.h"
#include "UserSettings.h"
#include "Logger.h"

// Used for serialization, and some globals

class ThreadPool;

class ProgramState
{
public:
	ProgramState(ThreadPool *thpool); // can be NULL
	~ProgramState();

	void AddCmd(const std::string& cmd, const std::string& params, bool early = false);

	bool HandleCommandlineCmd(const char *str);

	bool Load(const std::string& fn);
	bool SaveState(const std::string& fn);
	void HookSignals();
	void SaveData();
	bool Prepare();

	bool Init(IProblemSolver& wobj);

	// ProteinHolder proxy methods
	void SetGroupNames(const std::string& a, const std::string& b);
	bool ReadBLASTMatrix(const std::string& fn);
	bool ReadBLASTPairList(const std::string& fn, int left_group);
	bool ReadGraphletSigs(const std::string& fn, uint group);
	bool ReadNetwork(const std::string& fn);
	bool ReadNetworkSIF(const std::string& fn, uint groupIdx); // for easy cytoscape import
	bool ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader);
	bool ReadSeqSimData(const std::string& fn, uint groupIdx);

	// Checks abort criterions
	bool IsDone();

	void NextIteration(); // to be called once per iteration
	uint GetRunningTime() const; // in seconds
	uint GetTotalRunningTime() const { return GetRunningTime() + _totalRunningTime; }
	uint GetIterationCount() const { return _iterationCount; }

	inline const IProblemSolver *GetWorkObject() const { return workObject; }

	void Shutdown(); // Must be called before a clean exit

	ProteinHolder ph;
	DataHolder dh;
	UserSettings settings;

	static ProgramState& GetInstance();

	static bool MustQuit() { return _mustQuit; }

	const time_t *GetStartTime() const { return &_startTime; }

	static bool _Cmd_GRPN(const std::string& params);
	
	static bool _Cmd_NTW(const std::string& params);
	static bool _Cmd_NTWSIF(const std::string& params);
	static bool _Cmd_NTWEDGELIST(const std::string& params);
	static bool _Cmd_NTWEDGELISTHEADER(const std::string& params);
	
	static bool _Cmd_BLASTMTX(const std::string& params);
	static bool _Cmd_BLASTPAIRLIST(const std::string& params);
	static bool _Cmd_SEQSIM(const std::string& params);

	static bool _Cmd_GRSIGS(const std::string& params);
	
	static bool _Cmd_PREPARE(const std::string& params);
	static bool _Cmd_V(const std::string& params);
	static bool _Cmd_ITERATIONS(const std::string& params);
	static bool _Cmd_RUNTIMESECS(const std::string& params);


protected:

	static void _OnSignal(int);
	static bool _mustQuit;
	static bool _isSaving;
	static bool _saveOnQuit;

	bool _ExecOneLine(const std::string& line);
	bool _ExecCommand(const std::string& cmd, const std::string& params);

	void _AddToManualList(const std::string& s);

	uint _iterationCount;
	time_t _startTime;
	size_t _iterationsWithoutScoreChange;
	score _lastScore;
	uint _startIterationCount;
	uint _totalRunningTime; // accumulated from all prev states, but not current
	uint _lastSaveTime;
	uint _seriesCounter;
	IProblemSolver *workObject;

	std::list<std::string> _saveList, _earlySaveList; // cleared on every save
	std::list<std::string> _manualList; // filled when calling functions manually / creating a state
	std::list<std::string> _overrideList; // for commandline args

	Logger _logger;
	ThreadPool *thpool;

	static ProgramState *_instance;
};

#endif
