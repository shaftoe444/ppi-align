#include "common.h"
#include "Threading.h"
#include "PlatformSpecific.h"
#include "MersenneTwister.h"

#ifdef USE_SDL2
#  include <SDL.h>
#else
#  include <SDL/SDL.h>
#endif


// --------- Lockable ----------

Lockable::Lockable()
{
	_mtx = SDL_CreateMutex();
}

Lockable::~Lockable()
{
	SDL_DestroyMutex((SDL_mutex*)_mtx);
}

void Lockable::lock()
{
	SDL_LockMutex((SDL_mutex*)_mtx);
}

void Lockable::unlock()
{
	SDL_UnlockMutex((SDL_mutex*)_mtx);
}

// --------- Waitable ----------

Waitable::Waitable()
{
	_cond = SDL_CreateCond();
}

Waitable::~Waitable()
{
	SDL_DestroyCond((SDL_cond*)_cond);
}

void Waitable::wait()
{
	SDL_CondWait((SDL_cond*)_cond, (SDL_mutex*)_mtx);
}

void Waitable::signal()
{
	SDL_CondSignal((SDL_cond*)_cond);
}

void Waitable::broadcast()
{
	SDL_CondBroadcast((SDL_cond*)_cond);
}


class WorkerThread
{
public:
	WorkerThread(unsigned int id, ThreadPool& pool, unsigned int randomSeed, WorkerThread *prevThread)
		: _rng(randomSeed)
		, _id(id)
		, _pool(pool)
		, _threadPtr(NULL)
		, _prevThread(prevThread)
	{
	}

	void *run()
	{
		JobEntry je;
		do
		{
			_pool._fetchJob(je);
			//printf("Worker thread #%u starting job %p param %p\n", _id, je.func, je.param);
			if(je.func)
				je.func(_rng, je.param, je.param2, je.param3);
			//printf("Worker thread #%u finished.\n", _id);
			_pool._onJobEnd();
		}
		while(je.func);
		printf("Worker thread #%u exiting.\n", _id);
		return NULL;
	};

	void start()
	{
		ASSERT(!_threadPtr);

#if SDL_VERSION_ATLEAST(2, 0, 0)
		char buf[32];
		sprintf(buf, "W#%u", _id);
		_threadPtr = SDL_CreateThread(_Launch, buf, this);
#else
		_threadPtr = SDL_CreateThread(_Launch, this);
#endif
		if(!_threadPtr)
		{
			printf("ThreadPool ERROR: Failed to start thread!\n");
			exit(1);
		}
	}

	void join()
	{
		if(_threadPtr)
			SDL_WaitThread(_threadPtr, NULL);
		delete this;
	}

	static int _Launch(void *p)
	{
		WorkerThread *self = (WorkerThread*)p;
		printf("Worker thread #%u started.\n", self->_id);
		if(self->_prevThread)
			self->_prevThread->start();
		self->run();
		return 0;
	}

protected:

	MTRand _rng;

	unsigned int _id;
	ThreadPool& _pool;

	SDL_Thread *_threadPtr;
	WorkerThread *_prevThread;
};

ThreadPool::ThreadPool()
: _quit(false), _threadCount(0), _busyThreads(0)
{
	SDL_Init(SDL_INIT_TIMER | SDL_INIT_NOPARACHUTE);
}

void ThreadPool::start(size_t workers /* = 0 */)
{
	if(!workers)
	{
		workers = countCPUCores();
		printf("ThreadPool: Detected %u CPU cores\n", (unsigned int)workers);
	}
	printf("ThreadPool start: %u workers\n", (unsigned int)workers);
	_spawnWorkers(workers);
}

ThreadPool::~ThreadPool()
{
	quit(true);
	SDL_Quit();
}

void ThreadPool::_spawnWorkers(size_t count)
{
	ASSERT(count);

	MTRand seedGen; // auto-initializes by date and time
	//FOR DEBUGING MTRand seedGen(10); // auto-initializes by date and time

	_threadCount += count;

	WorkerThread *last = NULL;
	for(size_t i = 0; i < count; ++i)
	{
		WorkerThread *w =  new WorkerThread((unsigned int)i, *this, seedGen.randInt(), last);
		_th.push_back(w);
		last = w;
	}

	// Start all threads in chain.
	// This one will subsequently start the next one, etc.
	// This is to prevent valgrind/helgrind choking on thread creation.
	// Maybe SDL is doing something wrong, i.e. parallel thread creation messes up; not sure.
	last->start();
}


void ThreadPool::quit(bool wait /* = true */)
{
	if(_quit)
		return;

	_quit = true;

	JobEntry je;
	je.func = NULL;
	je.param = je.param2 = je.param3 = NULL;

	{
		MTGuard g(_lock);
		for(size_t i = 0; i < _th.size(); ++i)
			_jobs.push_back(je);
		_lock.broadcast();
	}

	if (wait)
	{
		for(size_t i = 0; i < _th.size(); ++i)
		{
			printf("ThreadPool: Join thread #%u\n", (uint)i);
			_th[i]->join();
		}
	}
	_th.clear();
}

void ThreadPool::waitUntilEmpty()
{
	MTGuard g(_lock);
	_lock.broadcast();
	while(_busyThreads || _jobs.size())
		_lock.wait();
}

bool ThreadPool::addJob(threadFunc f, void *param, void *param2, void *param3)
{
	MTGuard g(_lock);
	bool ret = addJobUnsafe(f, param, param2, param3);
	_lock.broadcast();
	return ret;
}

bool ThreadPool::addJobUnsafe(threadFunc f, void *param, void *param2, void *param3)
{
	if(_quit || !f)
	{
		printf("ThreadPool: AddJob %p -- IGNORED because quitting\n", f);
		return false;
	}
	//printf("ThreadPool: AddJob %p\n", f);
	JobEntry je;
	je.func = f;
	je.param = param;
	je.param2 = param2;
	je.param3 = param3;
	_jobs.push_back(je);
	return true;
}


void ThreadPool::assertIdle()
{
	MTGuard g(_lock);
	ASSERT(!_jobs.size());
	ASSERT(!_busyThreads);
}

void ThreadPool::_onJobEnd()
{
	MTGuard g(_lock);
	--_busyThreads;
	_lock.broadcast();
}

void ThreadPool::_fetchJob(JobEntry& e)
{
	MTGuard g(_lock);
	while(_jobs.empty())
		_lock.wait();
	e = _jobs.front();
	_jobs.pop_front();
	++_busyThreads;
}

bool ThreadPool::isIdle()
{
	MTGuard g(_lock);
	return !(_jobs.size() || _busyThreads);
}

