#ifndef GRAPHLET_SCORE_H
#define GRAPHLET_SCORE_H

#include "common.h"

class ProteinNode;

score calcGraphletDistance(const ProteinNode *a, const ProteinNode *b);

#endif
