#include "GraphletScore.h"
#include "ProteinNode.h"
#include <cmath>
#include <algorithm>


static const double graphlet_weights[GRAPHLET_SIGS] =
{
	1.0,
	0.838444532557004,
	0.838444532557004,
	0.676889065114007,
	0.743940642316373,
	0.676889065114007,
	0.743940642316373,
	0.743940642316373,
	0.582385174873377,
	0.624879821261446,
	0.515333597671011,
	0.546456463288587,
	0.44110990776397,
	0.42082970743038,
	0.368820463577819,
	0.676889065114007,
	0.582385174873377,
	0.624879821261446,
	0.676889065114007,
	0.624879821261446,
	0.582385174873377,
	0.582385174873377,
	0.676889065114007,
	0.676889065114007,
	0.487881284632746,
	0.44110990776397,
	0.384900995845591,
	0.582385174873377,
	0.46332435381845,
	0.42082970743038,
	0.46332435381845,
	0.515333597671011,
	0.42082970743038,
	0.46332435381845,
	0.487881284632746,
	0.546456463288587,
	0.487881284632746,
	0.42082970743038,
	0.46332435381845,
	0.402173731591086,
	0.269193833681854,
	0.29039710560496,
	0.29039710560496,
	0.29039710560496,
	0.402173731591086,
	0.42082970743038,
	0.313724084901656,
	0.301768886375453,
	0.259274239987384,
	0.384900995845591,
	0.402173731591086,
	0.29039710560496,
	0.339648030025923,
	0.259274239987384,
	0.231821926949119,
	0.223345528402594,
	0.353778130228015,
	0.158384326393324,
	0.223345528402594,
	0.158384326393324,
	0.123357261084647,
	0.192222662785018,
	0.249759642522892,
	0.164770349746754,
	0.19962249927108,
	0.128841638161964,
	0.065989729025416,
	0.0617900609595979,
	0.065989729025416,
	0.158384326393324,
	0.038067031828084,
	0.0131344759982275,
	0.00321488230375711
};

static const double graphlet_signatures_weights_sum = 30.24731991;

score calcGraphletDistance(const ProteinNode *a, const ProteinNode *b)
{
	ASSERT(a && b);
	ASSERT(a->proto->hasSigs);
	ASSERT(b->proto->hasSigs);
	const uint *grSigsA = a->proto->graphlets;
	const uint *grSigsB = b->proto->graphlets;
	const score *grX1 = a->proto->graphletLogs;
	const score *grX2 = b->proto->graphletLogs;

	double sum = 0;
	//double x1, x2;
	double y1, y2;
	//double sigA, sigB;

	for(uint i = 0; i < GRAPHLET_SIGS; ++i)
	{
		// OLD, SLOW CODE
		//sigA = grSigsA[i];
		//sigB = grSigsB[i];
		//x1 = log(sigA + 1.0);
		//x2 = log(sigB + 1.0); 
		//y1 = fabs(x1 - x2);
		//y2 = log(std::max(sigA, sigB) + 2.0);

		// Now partially precalculated in ProteinHolder.cpp
		y1 = fabs(grX1[i] - grX2[i]);
		y2 = log(std::max(grSigsA[i], grSigsB[i]) + 2.0);

		sum += (graphlet_weights[i] * y1 / y2);
	}
	sum /= graphlet_signatures_weights_sum;
	ASSERT(sum >= 0 && sum <= 1);
	return (score)sum;
}
