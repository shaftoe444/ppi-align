#ifndef RESULTPRINTER_H
#define RESULTPRINTER_H

#include "common.h"
#include <ostream>

class ProgramState;

namespace ResultPrinter {


void AsList(const ProgramState& state, std::ostream& o);




};

#endif
