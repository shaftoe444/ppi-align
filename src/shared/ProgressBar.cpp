#include <stdio.h>
#include "ProgressBar.h"

ProgressBar::~ProgressBar()
{
	printf("\n");
	fflush(stdout);
}

ProgressBar::ProgressBar(unsigned int totalcount /* = 0 */)
: total(totalcount), done(0), _perc(0), _oldperc(0)
{
	Update(true);
}

void ProgressBar::Step(bool force /* = false */)
{
	++done;
	Update(force);
}

void ProgressBar::Update(bool force /* = false */)
{
	if(total)
		_perc = (done * 100) / total;
	else
		_perc = 0;

	if(_perc > 100)
		_perc = 100;

	if(force || _oldperc != _perc)
	{
		_oldperc = _perc;
		Print();
	}
}

void ProgressBar::Print(void)
{
	printf("\r[");
	unsigned int i = 0, L = _perc / 2;
	for( ; i < L; i++)
		putchar('=');
	for( ; i < 50; i++)
		putchar(' ');
	printf("] %3u%% (%u/%u)\r", _perc, done, total);
	fflush(stdout);
}
