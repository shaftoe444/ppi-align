#include "common.h"
#include <algorithm>

#include "Evolution.h"
#include "ProgramState.h"
#include "ResultPrinter.h"


#include "Threading.h"
#include "CmdlineParser.h"
#include "CmdHelp.h"

#include <signal.h>
#include <fstream>

#include <stdio.h>  /* defines FILENAME_MAX */

void onSignal(int s)
{
	fprintf(stderr, "Caught signal %d\n", s);
	exit(s);
}

static void usage()
{
	puts("GEDEVO v0.1 -- Builtin help:");
	CmdHelp::Print();
	puts(
		"Example:\n"
		"$ ./gedevo --resume test.state --create --groups DM hprd --ntw DM.ntw\n"
		" --ntw hprd.ntw --grsig hprd.sigs hprd --grsig DM.sigs DM --undirected\n"
		" --pop 1000 --maxsame 500 --no-prematch\n"
		"\n"
		"See the README for additional information and input file format descriptions.\n"
		"Feel free to contact mmalek@mmci.uni-saarland.de for questions and comments."
		);
}

int main(int argc, char **argv)
{
	if(argc <= 1)
	{
		usage();
		return 0;
	}

	signal(SIGFPE, onSignal);

	const uint SHOW_BEST_AMOUNT = 5;

	ThreadPool thpool;
	ProgramState state(&thpool);

	CmdlineParser cmdline(argc, argv);
	if(!cmdline.Apply(state))
	{
		puts("Run the program with no parameters for a quick help.\n");
		return 2;
	}

	Evo evo(state.dh, &thpool);
	AgentMgr amgr(state.dh);
	evo.Link(amgr);

	bool good = state.Init(evo);
	if(good)
	{
		printf("Doing some evolution...\n");

		const AgentArray& arr = evo.GetAgents();
		bool perfect = false;
		bool stop = false;

		while(!(perfect || stop || ProgramState::MustQuit()))
		{
			state.NextIteration();
			evo.Update();

			printf("Done round #%u, %u agents alive\n", state.GetIterationCount(), (uint)arr.size());
			Agent *top = arr[0];
			const uint limit = std::min<uint>(SHOW_BEST_AMOUNT, arr.size());
			for(uint j = 0; j < limit; ++j)
			{
				Agent *a = arr[j];
				score ec = a->getEdgeCorrectness();
				perfect = perfect || (ec == 1);
				printf("%2u  GED: %f, EC: %f, PS: %f, raw GED: [%u, %u] (life: %u, DT: %u)\n",
					j, a->getGEDScore(), a->getEdgeCorrectness(), a->getPairSumScore(),
					a->getGEDRawFull(), a->getGEDRawPartial(), a->getLife(), a->getDistanceTo(top));
			}

			stop = state.IsDone();
		}

		if(perfect)
		{
			printf("Done! Found perfect solution after %u iterations.\n", state.GetIterationCount());
		}
		else if(stop)
		{
			printf("Done, abort criterion reached after %u iterations.\n", state.GetIterationCount());
		}

		state.Shutdown();
	}
	else
	{
		printf("Something isn't right, exiting. Re-check your settings.\n");
	}

	evo.Unlink();

	return good ? 0 : 1;
}
