#include "ProgramState.h"
#include "CmdlineParser.h"
#include "ProgressBar.h"


// Quick hack to dump the graphlet score matrix.
// Does in theory not depend on loaded networks,
// but the implementation of the surrounding
// environment, helper code, ... demands this. Sigh.

int main(int argc, char **argv)
{
    ThreadPool thpool;
    ProgramState state(&thpool);
    CmdlineParser cmdline(argc, argv);
    if(!cmdline.Apply(state))
        return 1;

    UserSettings settings;
    state.settings.varGroup = 0;
    state.settings.forceUndirectedEdges = 1; // important
    state.settings.pairWeightGraphlets = 1; // that it actually loads the matrix
    state.settings.weightGraphlets = 1;
    state.settings.weightNodeDist = 0;
    state.settings.pairWeightNodeDist = 0;
    state.settings.noWorkfiles = 1;
    state.settings.matchSameNames = 0;

    thpool.start(settings.numThreads);

    std::string fn;
    fn += state.ph.GetGroupName(0);
    fn += "_";
    fn += state.ph.GetGroupName(1);
    fn += ".grmtx";
    FILE *out = fopen(fn.c_str(), "w");

    if(!out)
    {
        printf("Error: Can't open output: %s\n", fn.c_str());
        exit(1);
    }

    state.Prepare();

    const NodeMapping& my = state.dh.GetDefaultMapping();
    const NodeMapping& ref = state.dh.GetRefMapping();
    const uint xmax = state.dh.GetMaxVariableNodeID();
    const uint ymax = state.dh.GetMaxReferenceNodeID();

    printf("\nWriting output file...\n");

    const uint bufsize = 1024 * 1024 * 50; // 50 MB
    char *buf = (char*)malloc(bufsize);
    {
        ProgressBar bar(ymax);
        setvbuf(out, buf, _IOFBF, bufsize); // write in large chunks to avoid disk seek bottlenecking

        for(uint y = 0; y < ymax; ++y)
        {
            const char *refname = ref.node[y]->name().c_str();
            const uint refid = ref.nodeId[y];
            for(uint x = 0; x < xmax; ++x)
            {
                score s = state.dh.GetGraphletScore(my.nodeId[x], refid);
                fprintf(out, "%s\t%s\t%.12f\n", my.node[x]->name().c_str(), refname, s);
            }
            bar.Step();
        }
    }
    fclose(out);
    free(buf);


    return 0;
}
