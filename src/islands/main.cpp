#include "common.h"
#include "ProgramState.h"
#include "World.h"

#include "Threading.h"
#include "CmdlineParser.h"

#include <signal.h>


void onSignal(int s)
{
	fprintf(stderr, "Caught signal %d\n", s);
	exit(s);
}


int main(int argc, char **argv)
{
	signal(SIGFPE, onSignal);

	ThreadPool thpool;
	ProgramState state(&thpool);

	// FIXME
	state.settings.maxAgents = 20;
	state.settings.varGroup = 0;
	state.settings.islands_startZoneIslands = 40;
	state.settings.islands_minZoneIterations = 0;
	state.settings.islands_zoneCheckInterval = 0;
	state.settings.islands_zoneGranularity = 0.02f;
	state.settings.islands_algoIterations[ALGO_EVO] = 7;
	state.settings.islands_algoIterations[ALGO_BEES] = 40;
	state.settings.islands_coarseThreadGranularity = 1;

	CmdlineParser cmdline(argc, argv);
	if(!cmdline.Apply(state))
		return 1;

	World world(state.dh, &thpool);

	bool good = state.Init(world);
	if(good)
	{
		bool stop = false;
		bool perfect = false;

		while(!(ProgramState::MustQuit() || stop || perfect))
		{
			state.NextIteration();
			world.Update();

			printf("Round #%u:\n", state.GetIterationCount());
			const std::vector<Zone*>& zv = world.getZones();
			for(size_t i = 0; i < zv.size(); ++i)
			{
				Zone *z = zv[i];
				const Agent *a = z->getBestAgent();
				if(a)
				{
					const score ec = a->getEdgeCorrectness();
					printf(" Zone %u [%.2f-%.2f]: %u isl | GED: %f, EC: %f, raw GED: [%u, %u] (life: %u)\n",
						i, z->getUpperLimit(), z->getLowerLimit(), (uint)z->size(),
						a->getGEDScore(), ec, a->getGEDRawFull(), a->getGEDRawPartial(), a->getLife());

					perfect = perfect || (ec == 1);
				}
				else
				{
					printf(" Zone %u [%.2f-%.2f]: %u isl | -----\n",
						i, z->getUpperLimit(), z->getLowerLimit(), (uint)z->size());
				}
			}

			stop = state.IsDone();
		}
	}
	else
	{
		printf("Something isn't right, exiting. Re-check your settings.\n");
	}

	state.Shutdown();

	return 0;
}
