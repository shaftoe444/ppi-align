#include "Island.h"
#include "DataHolder.h"
#include "World.h"
#include "UserSettings.h"
#include "Zone.h"
#include "ProgramState.h"

Island::Island(World *w)
 : _algo(NULL)
 , _amgr(NULL)
 , _zone(NULL)
 , _world(w)
 , _counterAlgo(0)
 , _counterZone(0)
 , _bestScore(-1)
 , _init_algoId(ALGO_EVO) // default algo ID
 , _firstInit(true)
{
	_amgr = new AgentMgr(w->getDataHolder());
}

Island::~Island()
{
	if(_algo)
	{
		_algo->Unlink();
		delete _algo;
	}
	delete _amgr;
}

void Island::Init()
{
	if(!_algo)
		ChangeAlgo(_init_algoId);

	if(!_firstInit)
	{
		_counterZone = 0;
		_counterAlgo = 0;
	}
	_firstInit = false;
}

void Island::Update()
{
	ASSERT(_algo);
	_algo->Update();
	_bestScore = _amgr->getBestAgent()->getScore();
	ASSERT(_bestScore >= 0 && _bestScore <= 1);

	++_counterAlgo;
	++_counterZone;

	uint need = _world->getDataHolder().GetUserSettings().islands_algoIterations[_algo->getAlgoID()];
	if(_counterAlgo >= need)
	{
		_counterAlgo = 0;
		ChangeAlgo(AlgoID((_algo->getAlgoID() + 1) % ALGO_MAX));
	}
}

void Island::serialize(ProgramState& state)
{
	char buf[64];
	state.AddCmd("ISLAND", "");

	sprintf(buf, "%u", _algo->getAlgoID());
	state.AddCmd("ALGO_ID", buf);

	sprintf(buf, "%u", _counterAlgo);
	state.AddCmd("COUNTER_ALGO", buf);

	sprintf(buf, "%u", _counterZone);
	state.AddCmd("COUNTER_ZONE", buf);

	_amgr->serialize(state);
}



bool Island::unserialize(const std::string& cmd, const std::string& params)
{
	if(cmd == "ALGO_ID")
	{
		_init_algoId = (AlgoID)atoi(params.c_str());
		return _init_algoId < ALGO_MAX;
	}

	if(cmd == "COUNTER_ALGO")
	{
		_counterAlgo = atoi(params.c_str());
		return true;
	}

	if(cmd == "COUNTER_ZONE")
	{
		_counterZone = atoi(params.c_str());
		return true;
	}

	return _amgr->unserialize(cmd, params);
}

void Island::ChangeAlgo(AlgoID algoid)
{
	if(_algo)
	{
		if(_algo->getAlgoID() == algoid)
			return;
		_algo->Unlink();
		_world->RetainAlgo(_algo);
	}
	_algo = _world->GetAlgo(algoid);
	ASSERT(_algo);
	_algo->Link(*_amgr);
	_algo->Init();
}

void Island::LevelUp()
{
	Zone *next = _zone->nextZone;
	_zone->Remove(this);
	if(!next)
		next = _world->NewZone();
	next->Add(this);
	_counterZone = 0;
}

bool Island::CanLevelUp() const
{
	return _counterZone > _world->getDataHolder().GetUserSettings().islands_minZoneIterations;
}

void Island::Gobble(Island *other)
{
	if(this == other) // that would be fatal.
		return;

	other->Destroy();

	AgentArray& my = _amgr->agents;
	AgentArray& oth = other->_amgr->agents;
	ASSERT(oth.size());
	my.reserve(my.size() + oth.size());
	for(size_t i = 0; i < oth.size(); ++i)
		my.push_back(oth[i]);
	oth.clear();

	// TODO: No health decay for a couple of iterations?
}

void Island::Destroy()
{
	_firstInit = true;
	_algo->Unlink();
	_zone->Remove(this);
	_world->RetainAlgo(_algo);
	_algo = NULL;
	_world->RetainIsland(this);
	_bestScore = -1;
}

