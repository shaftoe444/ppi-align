#ifndef ZONE_H
#define ZONE_H

class World;
class Island;
class World;
class Agent;

#include "Serializable.h"
#include <list>

class Zone : public Serializable
{
public:
	Zone(World *w, score beginScore);
	virtual ~Zone();

	void Init();
	void Update();
	void DoLevelup();

	void Add(Island *isl);
	void Remove(Island *isl);
	inline size_t size() const { return _islands.size(); }
	inline score getUpperLimit() const { return _scoreBegin; }
	inline score getLowerLimit() const { return _scoreEnd; }

	inline const std::list<Island*> getIslands() const { return _islands; }

	const Agent *getBestAgent() const;


	virtual void serialize(ProgramState& state);
	virtual bool unserialize(const std::string& cmd, const std::string& params);

	Zone *prevZone;
	Zone *nextZone;

protected:

	void _LimitSize();

	World *_world;
	std::list<Island*> _islands;
	score _scoreBegin;
	score _scoreEnd;
};

#endif
