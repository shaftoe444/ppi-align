#include "World.h"
#include "Zone.h"
#include "Island.h"
#include "Threading.h"
#include "ProgramState.h"

#include "BeeColony.h"
#include "Evolution.h"

World::World(const DataHolder& dh, ThreadPool *thpool)
 : _datah(dh)
 , _finePool(thpool)
 , _thpool(NULL)
 , _levelupCounter(0)
{
	_rng = new MTRand();
}

World::~World()
{
	for(size_t i = 0; i < _zones.size(); ++i)
		delete _zones[i];
	for(uint i = 0; i < ALGO_MAX; ++i)
		for(uint j = 0; j < _algoStore[i].size(); ++j)
			delete _algoStore[i][j];
	for(size_t i = 0; i < _deadIslands.size(); ++i)
		delete _deadIslands[i];
	delete _rng;
}

void World::Init()
{
	if(_datah.GetUserSettings().islands_coarseThreadGranularity)
	{
		_thpool = _finePool;
		_finePool = NULL;
		printf("World: Switched to coarse threading mode\n");
	}
	if(!_zones.size())
		NewZone(); // starting zone

	// TODO: want more initial zones?

	for(size_t i = 0; i < _zones.size(); ++i)
		_zones[i]->Init();

	if(_thpool)
		_thpool->waitUntilEmpty();
}

static void _InitIsland_MT(MTRand& /*unused*/, void *p, void *u1 = NULL, void *u2 = NULL)
{
	((Island*)p)->Init();
}

void World::InitIsland_deferred(Island *isl)
{
	RUN_JOB_AUTOMT(_InitIsland_MT, isl);
}

void World::Update()
{
	// Make sure Init() was called
	ASSERT(_zones.size());

	// Refill starting zone if necessary
	Zone *z = _zones[0];
	bool wait = false;
	while(z->size() < _datah.GetUserSettings().islands_startZoneIslands)
	{
		wait = true;
		Island *isl = GetIsland();
		InitIsland_deferred(isl);
		z->Add(isl);
	}
	if(wait && _thpool)
		_thpool->waitUntilEmpty();

	// Update all zones
	for(size_t i = 0; i < _zones.size(); ++i)
		_zones[i]->Update();

	// Wait until all calculations are done
	if(_thpool)
		_thpool->waitUntilEmpty();

	++_levelupCounter;
	if(_levelupCounter > _datah.GetUserSettings().islands_zoneCheckInterval)
	{
		_levelupCounter = 0;
		// The zone list may get modified here, if DoLevelup() needs to create a new zone.
		// Thus it's important we do NOT use an iterator here!
		for(size_t i = 0; i < _zones.size(); ++i)
			_zones[i]->DoLevelup();
	}
}

void World::RetainAlgo(AlgoBase *algo)
{
	MTGuard g(_algoLock);
	_algoStore[algo->getAlgoID()].push_back(algo);
}

AlgoBase *World::GetAlgo(AlgoID algoid)
{
	// Check if unused object exists that can be recycled
	{
		MTGuard g(_algoLock);

		if(_algoStore[algoid].size())
		{
			AlgoBase *ret = _algoStore[algoid].back();
			_algoStore[algoid].pop_back();
			return ret;
		}
	}

	// No? Create new.

	AlgoBase *algo = NULL;
	switch(algoid)
	{
		case ALGO_BEES: algo = new BeeColony(_datah, _finePool); break;
		case ALGO_EVO:  algo = new Evo(_datah, _finePool); break;
		default:
			ASSERT(false);
	}

	return algo;
}

void World::RetainIsland(Island *isl)
{
	_deadIslands.push_back(isl);
	// Can't ASSERT island empty here, because it's likely not - agents are copied off after; see Island::Gobble()
}

Island *World::GetIsland()
{
	if(_deadIslands.size())
	{
		Island *ret = _deadIslands.back();
		_deadIslands.pop_back();
		ASSERT(ret->getAgents().size() == 0);  // make sure there are no leftovers for some reason
		return ret;
	}

	return new Island(this);
}

Zone *World::NewZone()
{
	Zone *z = NULL;
	if(_zones.size())
	{
		Zone *lastz = _zones.back();
		z = new Zone(this, lastz->getLowerLimit());
		z->prevZone = lastz;
		lastz->nextZone = z;
	}
	else
	{
		z = new Zone(this, 1.0);
	}
	printf("World: Creating new zone, score = [%.4f .. %.4f]\n", z->getUpperLimit(), z->getLowerLimit());
	_zones.push_back(z);
	return z;
}

const Agent *World::getBestAgent() const
{
	return _zones.back()->getBestAgent();
}

bool World::IsEmpty() const
{
	return _zones.size() <= 1 && !_zones.back()->size(); // FIXME
}

void World::serialize(ProgramState& state)
{
	for(size_t i = 0; i < _zones.size(); ++i)
		_zones[i]->serialize(state);
}

bool World::unserialize(const std::string& cmd, const std::string& params)
{
	if(cmd == "ZONE")
	{
		NewZone();
		return true;
	}

	return _zones.back()->unserialize(cmd, params);
}
