#ifndef BEECOLONY_H
#define BEECOLONY_H

#include "common.h"
#include "Mutators.h"
#include "AlgoBase.h"
#include "ProgramState.h"

#include <algorithm>

class BeeColony : public AlgoBase
{
public:
	BeeColony(const DataHolder& dh, ThreadPool *thpool);
	virtual ~BeeColony();

	virtual void Init();
	virtual void Update();

	//send scouts and set workerss
	void initSwarm(uint workers, uint scouts);
	//method for mainloop; takes care of scouting and harvesting
	void doOptimization();
	//find and initialize new solutions and add them to "solutions"
	void sendScouts();
	//find and initialize new solutions with heuristics
	//only used during initilialization of bee swarm
	void sendHeuristicScouts();
	//harvest existing solutions
	void harvestSolutions();

	// FIXME: what is this used for, anyway?
	//inline score getWorstAbsolute() const { return (_settings.weightGraphlets + _settings.weightPairsum + _settings.weightGED); };
	inline score getWorstAbsolute() const { return _settings.getFinalWeightSum(); };

protected:
	virtual void _OnUnlink();

	//used for multithreading
	DEF_THREADFUNC(_Random_Init_MT);
	DEF_THREADFUNC(_Heuristic_Init_MT);
	DEF_THREADFUNC(_Naive_Greedy_Mutate_MT);
	DEF_THREADFUNC(_Greedy_Mutate_MT);
	DEF_THREADFUNC(_Randomised_Greedy_Mutate_MT);
	DEF_THREADFUNC(_Random_Mutate_MT);
	DEF_THREADFUNC(_Multi_Random_Mutate_MT);
	DEF_THREADFUNC(_Conservative_Mutate_MT);

	//oompares mutated agent with parent
	//decrease or increases health of mutated agent
	void _evaluate(Agent *mutated, Agent *parent);
	//discards worst solutions until number of solutions equals number of workers
	void _dropWorst();
	//drops weakest agents, starting with the dead ones
	void _dropWeakest();
	//sets all entries in _occupied to false
	void _resetOccupiedStates();
	//useful for getting best and worst agents
	void _sortAgents(AgentArray& arr);
	//decide if an agent should be picked
	bool _pick(Agent *agent);
	//degrades health and increases life of existing agents
	void _degradeAgent(Agent *agent);
	//drops decayed agents for good
	void _trimDecayed();
	//merge agents with mutated agents
	void _mergeAgentArrays();
	//returns decayed agents to agentarray, if scores get worse
	void _saveAgents(bool force = false);
	//sums up the socre of all agents and storees them in _scoreSum
	void _updateScoreSum();


	uint BEE_noWorkers;
	uint BEE_noScouts;
	
	//counter for number of iterations in which scores did not improve
	//once the counter reaches 10, randomised greedy mutation will be used
	uint _stuck;
	score _bestScore;
	score _scoreSum;
	//backup for agents dead agents with good scores
	AgentArray _decayed;
	//marks an agent that has been mutated with greedy methods
	//only used during harvesting
	std::vector<char> _occupied;
	//agents generated during optimization step
	AgentArray _agentsMutated;
	//needed for scouts
	MTRand rng;
	//drop duplicates every now and then
	uint _dropDupCounter;
};

#endif

