
#converts .ntw file to edge list

CYCLE(){

for NETWORKFILE in $NETWORKFILELIST
do
	FNAME=${NETWORKFILE##*/}
	FNAME_PURE=${FNAME%.ntw}
	#echo $NETWORKFILE $FNAME $FNAME_PURE
	echo "awk 'NR<2 {next} {print $2 \"\t\" $3}' $NETWORKFILE > $FNAME_PURE.edgelist"
	      awk 'NR<2 {next} {print $2 "\t"   $3}' $NETWORKFILE > $FNAME_PURE.edgelist
done
}

NETWORKFILELIST="*.ntw"
#NETWORKFILELIST="../data/networks/ppi_network_*.ntw"
CYCLE;

#NETWORKFILELIST="../data/testing/ppi_network_*.ntw" 

#NETWORKFILELIST="*.ntw"
