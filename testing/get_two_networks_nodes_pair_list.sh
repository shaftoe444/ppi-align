#!/bin/bash

#get edgelist netwroks as parameters
NET1=$1
NET2=$2

LIST1=`awk '{print $1 "\n" $2}' ${NET1} |  sort -u | grep -v -e "^$"`
LIST2=`awk '{print $1 "\n" $2}' ${NET2} |  sort -u | grep -v -e "^$"`

COUNTER=0
for node1 in $LIST1
do
	for node2 in $LIST2
	do
	
	echo -e "${node1}\t${node2}\t${COUNTER}"
	COUNTER=$(($COUNTER+1))
	done
done
